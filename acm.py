# -*- coding: utf-8 -*-
"""
acm.py
version 3.1
Created on Tue Oct  6 16:17:35 2020
first automate controle module
this module comunicate with pymodbustcp to VLHI PLC


@author: el jean-seb I.P.H.C. strasbourg 
jean-sebastien.pelle@iphc.cnrs.fr

"""

from pyModbusTCP.client import ModbusClient
from pyModbusTCP.server import ModbusServer
import socket
from threading import Thread
import time
from datetime import datetime
import sys

ERROR_dict = {0: "no error",
              -1: "undefined error",
              -2: "connection to PLC fail",
              -3: "PLC sending error",
              -4: "PLC recived error",
              -5: "PLC time out",
              -6: "PLC fram error",
              -7: "PLC unknown error",
              -8: "PLC crc error",
              -9: "input parameters error"
             }
   
#*****************************************************************************
#************************** class stdout_to_file *****************************
#******************** a class to store standard output in file ***************

class stdout_to_file(object):

    def __init__(self, filename, scr):
        """
        filname: file where monitor printing are store
        scr: if True show stdout on monitor
        
        declaration methode: sys.stdout = stdout_to_file(filename,scr)
        """
        self.to_scr = scr
        self.terminal = sys.stdout
        self.logfile = open(filename, "a")

    def write(self, message):
        if self.to_scr == True:
            self.terminal.write(message) #screen printing
            
        self.logfile.write(message) #file writing

    def flush(self):
        pass


#*****************************************************************************
#**************************** class dummy_plc ********************************
#********************a class to store standard output in file ****************

class dummy_plc(Thread):
    
    def __init__(self):
        """
        this class make a fake modbus server at the host address (127.0.0.1:502)
        this fake modbus server is for testing use only
        warning, it use threading methode see import threading methode an attribut
        """
        Thread.__init__(self)
        self.server = ModbusServer("127.0.0.1", 502, no_block = True)
        self.__in_run = False
    
        
    def run(self):
        """
        run: this methode mount the PLC fake server
             no return parameter
        """
        self.__in_run = True
        self.server.start()
        
        while self.__in_run == True:
            time.sleep(0.1)
            
        self.server.stop()
    
        
    def stop_server(self):
        """
        stop_server: this methode stop the fake PLC server
                     no return parameter
        """
        self.__in_run = False
    

#*****************************************************************************
#********************** class automat_control_class **************************
#***************a class for data transmisson to PLC in Python ****************

class automat_control_class (object):
    
    def __init__(self,__ip = '127.0.0.1', __log_file = "log_file.txt"):
        """
        This class is used for communicate with the PLC. It could also create
        a log file for modbus communication troubleshooting in debug mode

        Parameters
        ----------
        __ip : TYPE, optional
            DESCRIPTION. The default is '127.0.0.1'.
        __log_file : TYPE, optional
            DESCRIPTION. The default is "log_file.txt".

        Returns None.
        """
        
        self.mb_client = ModbusClient()
        self.mb_client.timeout(1)
        self.__log_file = __log_file
        self.__file_struc = ''
        self.__ip = __ip
        self.__port = 502
        self.__trace_state = False
        self.__code_error = 0
        self.__except_enable = True
    

    def __rise_error(self,error_value):
        """
        this method rise string error parameter
        the user does not need this methode

        Returns None.
        """
        
        self.__code_error = error_value
        if self.__except_enable == True:
          raise ValueError(ERROR_dict[error_value] + ", error N° :" + str(error_value))
  

    def __write_log_file(self, information):
        """
        This methode is used in debug mode for writing modbus operation 
        the user does not need this methode

        Parameters
        ----------
        information : string to write in log file

        Returns None.
        """
        self.__file_struc = open(self.__log_file, 'a')
        self.__file_struc.write(information)
        self.__file_struc.close() 


    def __catch_screen(self):
        """
        This metode write stdout in the log file in debug mode
        the user does not need this methode

        Returns None.
        """
        
        sys.stdout = stdout_to_file(self.__log_file, True)

        
    def __uncatch_screen(self):
        """
        This methode stop stdout in file writing
        the user does not need this methode

        Returns None.
        """
        sys.stdout.logfile.close()
        sys.stdout = sys.stdout.terminal

        
    def rise_exception(self,value):
        """
        this method enable or disable the rising exception
        True rise exception
        
        Returns None.
        """
        
        if value == True:
          self.__except_enable = True
          
        else:
          self.__except_enable = False


    def read_code_error(self):
        """
        This methode return a string with last error code 

        """
        return self.__code_error

        
    def trace_on(self):
        """
        Call this methode to set the systeme in debug mode

        Returns None.
        """
        self.__trace_state = True

        
    def trace_off(self):
        """
        Call this methode to discard the debug mode

        Returns None.
        """

        self.__trace_state = False


    def modbus_set_ip(self, ip):
        """
        with this method you can modify the ip address

        Parameters
        ----------
        ip : string with ip address

        Returns
        -------
        val : True in success case
        """
        
        try:
            socket.inet_aton(ip)
            self.__ip = ip
            val = True
        
        except socket.error :
            self.__ip = '127.0.0.1'
            val = False
        
        return val

    
    def open_modbus(self):
        """
        This method open communication with the PLC

        Returns True in success case
        """
        open_stat = True
        
        if self.__trace_state == True:
            self.__write_log_file('start comunication on {} @ {}\n'.format(self.__ip, datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
            self.mb_client.debug(True)
            self.__catch_screen()
        
        self.mb_client.host(self.__ip)
        self.mb_client.port(self.__port)
        
        if not self.mb_client.is_open():
            if not self.mb_client.open():
                print("unable to connect to {} : {}".format(self.__ip, str(self.__port)))
                open_stat = False
                self.__rise_error(-2)
        
        if self.__trace_state == True:
            self.__uncatch_screen() 
            
        return open_stat

    
    def close_modbus(self):
        """
        This methode close communication with the PLC

        Returns None.
        """
        
        if self.__trace_state == True:
            self.__catch_screen()
            
            self.mb_client.close()

            self.__uncatch_screen()
            self.__write_log_file('\nend of communication @ {}\n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
        
        else:
            self.mb_client.close()
           
            
    def plc_wr_single_register(self, add, value):
        """
        This methode is use to write a single 16 bit register in PLC

        Parameters
        ----------
        add (16 bit integer): address of the register
        value (16 bit integer) : data to write

        Returns True in success case
        """

        data_return = True
        self.__code_error = 0
        
        if (type(add) == int) and (type (value) == int):
            if self.__trace_state == True:
                self.__write_log_file('\nwrite single register @ {} \n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
                
                self.__catch_screen()
                if self.mb_client.write_single_register(add, value) != True:
                    self.__uncatch_screen()
                    data_return = False
                    self.__rise_error(self.mb_client.last_error() * -1)
                    
                else:    
                    self.__uncatch_screen()
                
            else:
                if self.mb_client.write_single_register(add, value) != True:
                   data_return = False
                   self.__rise_error(self.mb_client.last_error() * -1)
        
        else:
            data_return = False
            self.__rise_error(-9)
            
        return data_return
        
    
    def plc_wr_multi_registers(self, add, data_liste):
        """
        This methode is use to write many register

        Parameters
        ----------
        add (16 bit integer): address of the first register
        data_liste (list of 16 bit integer) : list of data to, write

        Returns
        -------
        data_return : True in success
        """
        
        data_return = True
        self.__code_error = 0
        
        if (type(add) == int)and (type(data_liste) == list):
            for i in data_liste :
                 if type(i) != int:
                     data_return = False
                     
            if data_return == True:
                if self.__trace_state == True:
                    self.__write_log_file('\nwrite multi registers @ {} \n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
                                       
                    self.__catch_screen()
                    if self.mb_client.write_multiple_registers(add, data_liste) != True:
                      self.__uncatch_screen()
                      data_return = False
                      self.__rise_error(self.mb_client.last_error() * -1)
                    
                    else:
                      self.__uncatch_screen()
                
                else:
                    if self.mb_client.write_multiple_registers(add, data_liste) != True:
                      data_return = False
                      self.__rise_error(self.mb_client.last_error() * -1)                    
            
        else:
            data_return = False
            self.__rise_error(-9) 

        return data_return                            
        

    def plc_wr_single_coil(self, add, bool_val):
        """
        This methode is use to write a binary output

        Parameters
        ----------
        add (16 bit integer): address of output bit         
        bool_val (boolean) : True or False

        Returns
        -------
        data_return : True in success
        """
        data_return = True
        self.__code_error = 0
        
        if (type(add) == int)and (type(bool_val) == bool):
            if self.__trace_state == True:
                self.__write_log_file('\nwrite signle coil @ {}\n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
                
                self.__catch_screen()
                if self.mb_client.write_single_coil(add, bool_val) != True:
                  self.__uncatch_screen()
                  data_return = False
                  self.__rise_error(self.mb_client.last_error() * -1)
                
                else:
                  self.__uncatch_screen()
                
            else:
                if self.mb_client.write_single_coil(add, bool_val) != True:
                  data_return = False
                  self.__rise_error(self.mb_client.last_error() * -1)
        
        else:
            data_return = False
            self.__rise_error(-9)    
            
        return data_return

    
    def plc_wr_multi_coil(self, add, data_liste):
        """
        This methode is use to write many binary output

        Parameters
        ----------
        add (16 bit integer): address of the first output bit
        data_liste (boolean list) : list of binary data to, write

        Returns
        -------
        data_return : True in success
        """
        data_return = True
        self.__code_error = 0
        
        if (type(add) == int) and (type (data_liste) == list):
            if self.__trace_state == True:
                self.__write_log_file('\nwrite multi coil @ {} \n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
                
                self.__catch_screen()
                if self.mb_client.write_multiple_coils(add, data_liste) != True:
                  self.__uncatch_screen()
                  data_return = False
                  self.__rise_error(self.mb_client.last_error() * -1)
                
                else:
                  self.__uncatch_screen()
                
            else:
                if self.mb_client.write_multiple_coils(add, data_liste) != True:
                  data_return = False
                  self.__rise_error(self.mb_client.last_error() * -1)
                
        else:
             data_return = False
             self.__rise_error(-9) 
            
        return data_return
            
  
    
    def plc_rd_input_registers(self, add, number):
        """
        this method is use to read read only register

        Parameters
        ----------
        add (16 bit integer): address of the first register
        number (integer): number of register to read

        Returns
        -------
        list_return : False in mistake input parameter, None if communication close
                      list of integer in success
        """

        
        if (type(add) == int) and (type(number) == int):
            if self.__trace_state == True:
                self.__write_log_file('\nread input registers @ {}\n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
                
                self.__catch_screen()
                list_return = self.mb_client.read_input_registers(add, number)                
                self.__uncatch_screen()
                if type(list_return) != list:
                   list_return= False
                   self.__rise_error(self.mb_client.last_error() * -1)
            
            else:
                list_return = self.mb_client.read_input_registers(add, number)
                if type(list_return) != list:
                   list_return= False
                   self.__rise_error(self.mb_client.last_error() * -1)
                
        else:
            list_return = False
            self.__rise_error(-9)
        
        return list_return
        
    
    def plc_rd_holding_registers(self, add, number):
        """
        this method is use to read register

        Parameters
        ----------
        add (16 bit integer): address of the first register
        number (integer): number of register to read

        Returns
        -------
        list_return : False in mistake input parameter, None if communication close
                      list of integer in success
        """
        
        
        if (type(add) == int) and (type(number) == int):
            if self.__trace_state == True:
                self.__write_log_file('\nread holding registers @ {}\n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
                
                self.__catch_screen()
                list_return = self.mb_client.read_holding_registers(add, number)
                self.__uncatch_screen()
                if type(list_return) != list:
                  list_return= False
                  self.__rise_error(self.mb_client.last_error() * -1)
            
            else:
                list_return = self.mb_client.read_holding_registers(add, number)
                if type(list_return) != list:
                  list_return= False
                  self.__rise_error(self.mb_client.last_error() * -1)
                
        else:
            list_return = False
            self.__rise_error(-9)
            
        return list_return     
    

    def plc_rd_discret_inputs(self, add, number):
        """
        This methode is use to read discret input (boolean)

        Parameters
        ----------
        add (16 bit integer): address of the first discret input
        number (integer): number of discret input to read

        Returns
        -------
        list_return : False in mistake input parameter, None if communication close
                      list of boolean in success
        """
        
        
        if (type(add) == int) and (type(number) == int):
            if self.__trace_state == True:
                self.__write_log_file('\nread discret input @ {}\n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
                
                self.__catch_screen()
                list_return = self.mb_client.read_discrete_inputs(add, number)
                self.__uncatch_screen()
                if type(list_return) != list:
                  list_return = False
                  self.__rise_error(self.mb_client.last_error() * -1)
                
            else:
                list_return = self.mb_client.read_discrete_inputs(add, number)
                if type(list_return) != list:
                  list_return = False
                  self.__rise_error(self.mb_client.last_error() * -1)

        else:
            list_return = False
            self.__rise_error(-9)
                
        return list_return
        

    def plc_rd_coil(self, add, number):
        """
        This methode is seam as plc_rd_discret_inputs() 
        use to read discret input (boolean)

        Parameters
        ----------
        add (16 bit integer): address of the first discret input
        number (integer): number of discret input to read

        Returns
        -------
        list_return : False in mistake input parameter, None if communication close
                      list of boolean in success
        """

        
        if (type(add) == int) and (type(number) == int):
            if self.__trace_state == True:
                self.__write_log_file('\nread coils @ {} \n'.format(datetime.now().strftime("%m/%d/%Y, %H:%M:%S")))
                
                self.__catch_screen()
                list_return = self.mb_client.read_coils(add, number)
                self.__uncatch_screen()
                if type(list_return) != list:
                  list_return = False
                  self.__rise_error(self.mb_client.last_error() * -1)
                
            else:
                list_return = self.mb_client.read_coils(add, number)
                if type(list_return) != list:
                  list_return = False
                  self.__rise_error(self.mb_client.last_error() * -1)

        else:
            list_return = False
            self.__rise_error(-9)
                
        return list_return



    
if __name__ == "__main__":

    """
    test protocol for PLC control module 

    """


    client_tst = automat_control_class('127.0.0.1', 'tst_report4.txt')    
    server_tst = dummy_plc()
    
    add_sr = 100
    val_sr = 5
    
    add_mulr = 101
    val_mulr = [21, 27, 78, 9, 30]
    
    add_sc = 10
    val_sc = True
    
    add_mulc = 11
    val_mulc = [True, True, False, True]
    
    print ("test program for module acm")
    
       
    server_tst.start()
    print ("plc server for test munting")


    client_tst.trace_on()
    time.sleep(0.5)

    client_tst.rise_exception(False)


    print ("open modbus comunication")
    
    try:
        print("ouverture connection: ",client_tst.open_modbus())
        
    except ValueError:
        print("type d'erreur: ", client_tst.read_code_error())
        
            
    print("writing datas coil and register")
    
    print("try to write register from 100", val_sr, val_mulr)
    try:
      print("ecriture single registre: ",client_tst.plc_wr_single_register(add_sr, val_sr))

    except ValueError:
      print("fail to write single register / type d'erreur: ", client_tst.read_code_error())

    
    try :
      print("ecriture multi registre: ",client_tst.plc_wr_multi_registers(add_mulr, val_mulr))
      
    except ValueError:
      print("fail to write multi register / type d'erreur: ", client_tst.read_code_error())


    print("try to write coil from 10", val_sc, val_mulc)
    try :
      print("ecriture single coil: ",client_tst.plc_wr_single_coil(add_sc, val_sc))

    except ValueError:
      print("fail to write single coil / type d'erreur: ", client_tst.read_code_error())    

    
    try:
      print ("ecriture multi coil: ",client_tst.plc_wr_multi_coil(add_mulc, val_mulc))

    except ValueError:
      print("fail to write multi coil / type d'erreur: ", client_tst.read_code_error())

    print("read coil and register")


    try:
      rd_coil = client_tst.plc_rd_coil(add_sc, 5)
      print("read 5 coil from add 10:", rd_coil)

    except ValueError:
      print("fail to read multi register / type d'erreur: ", client_tst.read_code_error())
      
      
    try:
      rd_register = client_tst.plc_rd_input_registers(add_sr, 6)
      print("read 6 register from add 100:", rd_register)

    except ValueError:
      print("fail to read multi register / type d'erreur: ", client_tst.read_code_error())


    print("other read tests")

    try:
      print(client_tst.plc_rd_holding_registers(add_sr, 6))
    
    except ValueError:
      print("fail to read holding register / type d'erreur: ", client_tst.read_code_error())
    
    
    try:
      print(client_tst.plc_rd_discret_inputs(add_sc, 5))
      
    except ValueError:
      print("fail to read discret input / type d'erreur: ", client_tst.read_code_error())
        



    print("close modbus")
    client_tst.close_modbus()
    
    print("shutdown server")
    server_tst.stop_server()

    server_tst.join()
    time.sleep(0.5)



    
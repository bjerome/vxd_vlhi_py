# Questions:

## Q-1-3-B

> How many IOCs access the PLC? Is the just the VLHI IOC or are there others?
> **Current understanding:** By definition, the VLHI-IOC should be the only code interfacing with the PLC.

Basically only the VLHI-IOC should access the PLC. However, we should verify, if multiple connections via modbus are possible. This would allow to split the IOC into multiple ones in case of performance issues. Please perform a proof of concept, if more clients can connect to the HW modbus interface and perform `IO`s! It will have an impact on the design of the system, regarding scalability.

## Q-2-2-A

> Do we agree that there are no other PVs to be handled by the VLHI IOC than those listed in sections 5.1)?

This question is related to the functionality of the VLHI HW. Can all HW features be utilized/monitored with the registers defined in section 5.1?
The PVs provided by the system should match ones defined in the [VLHI OPI](https://stash.desy.de/projects/B2SVD/repos/svd_sc_opi/browse/env_opi/VLHI/VLHIMonitor.opi) as well as the state machine PVs.

## Q-2-2-B

> For the VLHI-IOC the PV prefix for all PV is VXD:VLHI:, is it case sensitive?

It is case sensitive.

## Q-3-1-X

Should cover the questions on user request and the interlock events.

The black dashed arrows, in the state machine figure, are user inputs via the `VXD:VLHI:Request` PV. The red dashed lines are events triggered by SVD LV/HV interlock output registers. Both of them can cause the state machine to perform transitions.

`SVD:Env` is the main controller/user distributing the request from the shifter to each env. module. The common state and request is defined in the `env_defines.py`, in the example FSM example code.

In addition to the `SVD:Env` the VLHI IOC excepts custom request issued by experts. Those request `VLHIRequest` are defined as an extension to `EnvRequest`, see `vlhi_defines.py`. The `VLHIRequest` contains custom commands to manually issue and release the SVD LV/HV interlocks.

Note: The `ERROR` state is reachable at any state/transition. It is triggered by an unrecoverable error that requires shifter/expert ack. and action. In case of VXD:VLHI:

- a modbus disconnection later longer than 1 sec for example.
- if the source of the error recovers we will still stay in `ERROR` state and just continue monitoring registers and updating the heart beat (It's automated with the modbus lib., once the connection recovers the coroutines must continue automatically.)

### Notes

- OPIs are not the only source of client connections!


## Q-3-1-B/D
> Is it correct to assume that the XML configuration is loaded just once during the CONFIGURING transition? Which means that to load a different configuration, the user needs to ABORT first and then CONFIGURE again.

Yes it's required! The change in state indicates a reconfiguration of the system (`X -> ABORTING -> IDLE -> CONFIGURING -> READY`). The XML file given `by SVD:Env:XML` must be (re)parsed at `START` request in the state `READY`. The configuration in turn are again uploaded to the FW! The final state is of the latter transition is `RUNNING`.

## Q-3-1-E
> Are the following assumptions about access requests to PVs depending on the state machine correct or not.
>
> 1. The two PVs related to the state machine should be handled by the IOC in any state.
> 2. The IOC updates the PV corresponding to a read request for a PLC register in any state.
> 3. The IOC never modifies a PLC register by write access unless in the CONFIGURING state (and then only for the restricted list of registers presented in section 5.2)).

1. The 2 PVs are representing the state machine thus must be handled correspondingly!
2. In any state the registers are monitored unless a sufficiently long disconnection (~1 sec) to the VLHI HW triggers the `ERROR` state. On the automatic recovery of the modbus connection the system must resume monitoring as well a updating the heart beat even in the `ERROR` state!
3. The registers are modified upon the following valid requests:
    - `CONFIGURE`: uploading the configuration recorded in the XML file.
    - `START`: uploading the configuration recorded in the XML file.
    - `ACK_ALARM`: releasing the SVD LV/HV HW interlock, if the corresponding output is active.
    - `INTERLOCK_LV`: interlocks the SVD LV.
    - `INTERLOCK_HV`: interlocks the SVD HV.
    - `RELEASE_LV`: releases the SVD LV.
    - `RELEASE_HV`: releases the SVD HV.
    - during any state updating the heart beat!

## Q-3-1-F and Q-3-1-G

> How is detected the “LV inhibit event”, by testing in polling the PLC “SVD LV Enable” output?
> How is detected the “HV inhibit event”, by testing in polling the PLC “SVD HV Enable” output?

If a hardware interlock event of the corresponding SVD LV/HV is detected, it must trigger the transition to the corresponding state (`HV/LVINHIBIT`) from `RUNNING`. SVD LV interlock overrules the HV one!

### Notes
The polling rate and update time is not a big issue since `SVD:PS` will detect the HW interlock signals and react correspondingly w/o the `VXD:VLHI` input. The `VXD:VLHI:State` will also trigger the same SVD:PS recovery function, but just as a software back in case of HW malfunctions.

## Q-3-2-A

> The XML file does not change during standard PLC operation (state machine in RUNNING state), so it is not necessary to reload it periodically, correct?

Continues reloading would cause data races on file system since the user can modify the configuration at any time. The XML file is parsed upon a valid `CONFIGURE` and `START` request.

## Q-3-2-B

> The VLHI IOC only reads through caproto the SVD:Env:XML to get the configuration file name, correct?

Yes, the configuration file path is provide by the `SVD:Env:XML`.

## Q-3-2-C

> How SpikeFilter is used by the IOC but it doesn’t seem to be a PLC parameter?

see, `VXD:VLHI:e_spike_window_r` and `VXD:VLHI:e_uptime_threshold_r`.
